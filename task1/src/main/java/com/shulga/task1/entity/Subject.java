package com.shulga.task1.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "subject")
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "subjects")
    private Set<Group> groups;

    @ManyToMany(mappedBy = "subjects")
    private Set<RecordBook> record_books;

    public Subject() {}

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<RecordBook> getRecord_books() {
        return record_books;
    }

    public void setRecord_books(Set<RecordBook> record_books) {
        this.record_books = record_books;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}